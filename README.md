# discord-module-logger

## Environment

| Key                  | Description                                                             |
|----------------------|-------------------------------------------------------------------------|
| ARTIFACT_EXPORT_PATH | Path relative to the project to copy artifact after rollup compilation. |