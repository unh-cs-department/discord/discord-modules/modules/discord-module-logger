import { Module } from "@unh-csonline/discord-modules";
import MessageUpdateMetaFilter from "filters/messagefilters/MessageUpdateMetaFilter";
import MessageMetadataTransform from "transforms/messagetransforms/MessageMetadataTransform";
import MessageUpdateMetaTransform from "transforms/messagetransforms/MessageUpdateMetaTransform";
import MessageUpdateMongoDBTransporter from "transporters/mongodb/messagetransporters/MessageUpdateMongoDBTransporter";
import Logger, { LoggerOptions, LoggerType } from "../Logger";

class MessageUpdateMetaContentLogger extends Logger {
    constructor(module: Module){
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MessageUpdateEvent,
            filter: new MessageUpdateMetaFilter(),
            transforms: [new MessageMetadataTransform(), new MessageUpdateMetaTransform()],
            transporters: [new MessageUpdateMongoDBTransporter({module: module})]
        }

        super(options)
    }
}

export default MessageUpdateMetaContentLogger