import { Module } from "@unh-csonline/discord-modules";
import Logger, { LoggerOptions, LoggerType } from "loggers/Logger";
import MessageDeleteMongoDBTransporter from "transporters/mongodb/messagetransporters/MessageDeleteMongoDBTransporter";
import MessageDeleteTransform from "transforms/messagetransforms/MessageDeleteTransform";
import MessageDeleteFilter from "filters/messagefilters/MessageDeleteFilter";
import MessageMetadataTransform from "transforms/messagetransforms/MessageMetadataTransform";

class MessageDeleteLogger extends Logger {
    constructor(module: Module) {
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MessageDeleteEvent,
            filter: new MessageDeleteFilter(),
            transforms: [new MessageMetadataTransform(), new MessageDeleteTransform()],
            transporters: [
                new MessageDeleteMongoDBTransporter({module: module})
            ]
        };

        super(options);
    }
}
export default MessageDeleteLogger;
