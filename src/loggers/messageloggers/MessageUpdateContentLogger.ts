import { Module } from "@unh-csonline/discord-modules";
import MessageContentFilter from "filters/messagefilters/MessageContentFilter";
import DiscordMessageEmbedFormatter from "formatters/discord/DiscordMessageEmbedFormatter";
import MessageContentTransform from "transforms/messagetransforms/MessageContentTransform";
import DiscordRelativeTransporter from "transporters/discord/DiscordRelativeTransporter";
import Logger, { LoggerOptions, LoggerType } from "loggers/Logger";

class MessageUpdateContentLogger extends Logger {
    constructor(module: Module) {
        const options: LoggerOptions = {
            module: module,
            type: LoggerType.MessageUpdateEvent,
            filter: new MessageContentFilter(),
            transforms: [new MessageContentTransform()],
            transporters: [
                new DiscordRelativeTransporter({
                    module: module,
                    channelName: "log",
                    guildIdProperty: "guildId",
                    formatter: DiscordMessageEmbedFormatter
                })
            ]
        };

        super(options);
    }
}

export default MessageUpdateContentLogger;
