import { Module } from "@unh-csonline/discord-modules";
import { Message } from "discord.js";
import { LoggerType } from "loggers/Logger";
import LoggerRepository from "repositories/LoggerRepository";
import LoggingListener from "../LoggingListener";

export default class MessageUpdateListener extends LoggingListener<"messageUpdate"> {
    constructor(module: Module, loggerRepository: LoggerRepository) {
        super(module,loggerRepository, LoggerType.MessageUpdateEvent, "messageUpdate");
    }

    async execute(oldMessage: Message, newMessage: Message): Promise<void> {
        await this.log(newMessage).catch(console.error);
    }
}