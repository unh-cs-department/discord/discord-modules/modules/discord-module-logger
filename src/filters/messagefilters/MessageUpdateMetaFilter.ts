import MessageMetaFilter from "./MessageMetaFilter";

class MessageUpdateMetaFilter extends MessageMetaFilter {
    constructor(){
        super (["editedAt"])
    }
}

export default MessageUpdateMetaFilter