import Filter, { FilterOptions } from "../Filter";

export default abstract class MessageMetaFilter extends Filter {
    constructor(options: Array<string>) {
        const keys = [
            "id",
            "guild",
            "channel",
            "author",
            "type",
            "mentions"
        ]

        const opt: FilterOptions = {
            keys: [keys, options].flat()
        };

        super(opt);
    }
}