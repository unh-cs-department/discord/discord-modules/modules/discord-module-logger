import { filter, MappedObject } from "utils/FilterUtil";

interface FilterOptions {
    keys: string[];
}

/**
 * Filter the properties in a key-value object.
 */
abstract class Filter {
    private readonly keys: string[];

    protected constructor(options: FilterOptions) {
        this.keys = options.keys;
    }

    public apply(data: MappedObject): MappedObject {
        return filter(data, this.keys);
    }
}

export default Filter;

export { FilterOptions };
