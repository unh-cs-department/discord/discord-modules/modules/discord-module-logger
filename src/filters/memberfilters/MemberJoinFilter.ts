import MemberFilter from "./MemberFilter";

export default class MemberJoinFilter extends MemberFilter {
    constructor(){
        super(["joinedAt"])
    }
}