import Filter, { FilterOptions } from "filters/Filter";

abstract class MemberFilter extends Filter {
    constructor(options: Array<string>) {
        const keys = [
            "user",
            "guild",
        ]
        const opt: FilterOptions = {
            keys: [keys, options].flat()
        };

        super(opt);
    }
}

export default MemberFilter;
