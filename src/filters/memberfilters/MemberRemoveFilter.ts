import MemberFilter from "./MemberFilter";

export default class MemberRemoveFilter extends MemberFilter {
    constructor() {
        super (["removedAt"])
    }
}