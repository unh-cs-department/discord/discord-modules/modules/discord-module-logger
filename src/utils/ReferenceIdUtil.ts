import { v1 } from "uuid";

type ReferenceIdType = string;

const ReferenceId = () => v1();

export { ReferenceIdType, ReferenceId };
