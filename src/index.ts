import { Client, Module, ModuleManifest } from "@unh-csonline/discord-modules";
import MessageListener from "listeners/messagelisteners/MessageListener";
import MessageEditListener from "listeners/messagelisteners/MessageEditListener"
import BaseMessageContentLogger from "loggers/messageloggers/BaseMessageContentLogger";
import BaseMessageMetaLogger from "loggers/messageloggers/BaseMessageMetaLogger";
import MessageUpdateContentLogger from "loggers/messageloggers/MessageUpdateContentLogger";


import LoggerRepository from "repositories/LoggerRepository";
import MemberJoinLogger from "loggers/memberloggers/MemberJoinLogger";
import MemberJoinListener from "listeners/memberlisteners/MemberJoinListener";
import MessageDeleteLogger from "loggers/messageloggers/MessageDeleteLogger";
import MessageDeleteListener from "listeners/messagelisteners/MessageDeleteListener";
import MemberRemoveLogger from "loggers/memberloggers/MemberRemoveLogger";
import MemberRemoveListener from "listeners/memberlisteners/MemberRemoveListener";
import MessageUpdateMetaLogger from "loggers/messageloggers/MessageUpdateMetaLogger";

export default class LoggerModule extends Module {
    private readonly loggerRepository: LoggerRepository;

    constructor(client: Client, manifest: ModuleManifest) {
        super(client, manifest);

        this.loggerRepository = new LoggerRepository();
    }

    public async register(): Promise<void> {
        console.info("INFO: Registered discord-module-logger.");

        this.loggerRepository.add(new BaseMessageMetaLogger(this));
        this.loggerRepository.add(new BaseMessageContentLogger(this));
        this.loggerRepository.add(new MessageUpdateContentLogger(this));
        this.loggerRepository.add(new MemberJoinLogger(this));
        this.loggerRepository.add(new MemberRemoveLogger(this));
        this.loggerRepository.add(new MessageDeleteLogger(this));
        this.loggerRepository.add(new MessageUpdateMetaLogger(this));

        await Promise.all([
            this.client.listenerManager.register(
                new MessageListener(this, this.loggerRepository)),
            this.client.listenerManager.register(
                new MessageEditListener(this, this.loggerRepository)),
            this.client.listenerManager.register(
                new MemberJoinListener(this, this.loggerRepository)),
            this.client.listenerManager.register(
                new MemberRemoveListener(this, this.loggerRepository)),
            this.client.listenerManager.register(
                new MessageDeleteListener(this, this.loggerRepository))
        ])

        return Promise.resolve();
    }
}
