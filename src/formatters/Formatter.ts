import { MessageOptions } from "discord.js";
import { MappedObject } from "utils/FilterUtil";

declare type Formatter =
    | StringFormatter
    | DiscordMessageEmbedFormatter
    | NullFormatter;

declare type StringFormatter = (object: MappedObject) => string;
declare type DiscordMessageEmbedFormatter = (
    object: MappedObject
) => MessageOptions;
declare type NullFormatter = (object: MappedObject) => MappedObject;

export { Formatter, StringFormatter, DiscordMessageEmbedFormatter };
