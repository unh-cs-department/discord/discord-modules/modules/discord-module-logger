import { model, Model, Schema } from "mongoose";
import MemberMongoDBTransporter, { IMemberMeta, MemberMetaMongoDBTransporterOptions, MemberMetaSchemaBase } from "./MemberMongoDBTransporter";

interface IMemberRemoveMeta extends IMemberMeta {
    removedAt: Date;
}

const MemberRemoveMetaSchema: Schema = new Schema({
    ...MemberMetaSchemaBase, 
    removedAt: {
        type: Date,
        required: true
    }
})

const MemberRemoveMeta: Model<IMemberRemoveMeta> = model(
    "MemberRemoveMeta",
    MemberRemoveMetaSchema
);

class MemberRemoveMongoDBTransporter extends MemberMongoDBTransporter {
    constructor(options: MemberMetaMongoDBTransporterOptions) {
        super(options, MemberRemoveMeta)
    }
}

export default MemberRemoveMongoDBTransporter;
