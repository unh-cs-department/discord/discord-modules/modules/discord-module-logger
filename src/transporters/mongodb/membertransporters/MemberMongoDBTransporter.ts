
import { Module } from "@unh-csonline/discord-modules";
import { Model } from "mongoose";
import MongoDBTransporter from "transporters/mongodb/messagetransporters/MongoDBTransporter";

interface IMemberMeta extends Document {
    referenceId: string;
    id: string;
    guild: {
        id: string | null;
        name: string | null;
    };
}

const MemberMetaSchemaBase = {
    referenceId: {
        type: String,
        required: true
    },
    user: {
        id: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        },
        nickname: {
            type: String,
            required: true
        }
    },
    guild: {
        id: {
            type: String,
            required: false
        },
        name: {
            type: String,
            required: false
        }
    }
}

interface MemberMetaMongoDBTransporterOptions {
    module: Module;
}

abstract class MemberMongoDBTransporter extends MongoDBTransporter {
    constructor(options: MemberMetaMongoDBTransporterOptions, model: Model<any>) {
        super({
            module: options.module,
            model: model
        });
    }
}

export default MemberMongoDBTransporter;

export {
    MemberMetaMongoDBTransporterOptions,
    IMemberMeta,
    MemberMetaSchemaBase
};
