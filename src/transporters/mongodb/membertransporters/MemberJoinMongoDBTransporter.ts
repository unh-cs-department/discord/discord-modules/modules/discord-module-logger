import { model, Model, Schema } from "mongoose";
import MemberMongoDBTransporter, { IMemberMeta, MemberMetaMongoDBTransporterOptions, MemberMetaSchemaBase } from "./MemberMongoDBTransporter";

interface IMemberJoinMeta extends IMemberMeta {
    joinedAt: Date
}

const MemberJoinMetaSchema: Schema = new Schema({
    ...MemberMetaSchemaBase, 
    joinedAt: {
            type: Date,
            required: true
    }
})

const MemberJoinMeta: Model<IMemberJoinMeta> = model(
    "MemberJoinMeta",
    MemberJoinMetaSchema
);

class MemberJoinMongoDBTransporter extends MemberMongoDBTransporter {
    constructor(options: MemberMetaMongoDBTransporterOptions) {
        super(options, MemberJoinMeta)
    }
}

export default MemberJoinMongoDBTransporter;

