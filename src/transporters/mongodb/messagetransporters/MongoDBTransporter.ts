import { Module } from "@unh-csonline/discord-modules";
import { connect, Model } from "mongoose";
import { MappedObject } from "utils/FilterUtil";
import Transporter from "transporters/Transporter";

interface MongoDBTransporterOptions {
    module: Module;
    model: Model<any>;
}

abstract class MongoDBTransporter extends Transporter {
    public readonly module: Module;
    public readonly model: Model<any>;

    protected constructor(options: MongoDBTransporterOptions) {
        super({});

        this.module = options.module;
        this.model = options.model;
    }

    public async transport(
        originalObject: MappedObject,
        refinedObject: MappedObject
    ): Promise<void> {
        await connect(process.env.MONGODB_URI as string);
        await this.model.create(refinedObject).catch(console.error);

        return Promise.resolve();
    }
}

export default MongoDBTransporter;

export { MongoDBTransporterOptions };
