import { Model, model, Schema } from "mongoose";
import MessageMetaMongoDBTransporter, { IMessageMeta, MessageMetaMongoDBTransporterOptions, MessageMetaSchemaBase } from "./MessageMetaMongoDBTransporter";

interface IMessageUpdate extends IMessageMeta {
    updatedAt: Date
}

const MessageUpdateMeta = new Schema({
    ...MessageMetaSchemaBase,
    editedAt: {
        type: Date,
        required: true
    }
})

const MessageUpdate: Model<IMessageUpdate> = model(
    "MessageUpdateMeta",
    MessageUpdateMeta
)

class MessageUpdateMongoDBTransporter extends MessageMetaMongoDBTransporter {
    constructor(options: MessageMetaMongoDBTransporterOptions) {
        super(options, MessageUpdate)
    }
}

export default MessageUpdateMongoDBTransporter