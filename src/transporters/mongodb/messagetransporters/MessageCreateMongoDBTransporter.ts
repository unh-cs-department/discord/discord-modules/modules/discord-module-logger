import { model, Model, Schema } from "mongoose";
import MessageMetaMongoDBTransporter, { IMessageMeta, MessageMetaMongoDBTransporterOptions, MessageMetaSchemaBase } from "./MessageMetaMongoDBTransporter";

interface IMessageCreateMeta extends IMessageMeta {
    createdAt: Date;
}

const MessageCreateMetaSchema: Schema = new Schema({
    ...MessageMetaSchemaBase,
    createdAt: {
        type: Date, 
        required: true
    }
});

const MessageCreateMeta: Model<IMessageCreateMeta> = model(
    "MessageCreateMeta",
    MessageCreateMetaSchema
);


class MessageCreateMetaMongoDBTransporter extends MessageMetaMongoDBTransporter {
    constructor(options: MessageMetaMongoDBTransporterOptions) {
        super(options, MessageCreateMeta);
    }
}

export default MessageCreateMetaMongoDBTransporter;
