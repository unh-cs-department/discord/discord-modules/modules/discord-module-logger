import { Module } from "@unh-csonline/discord-modules";
import { Channel, Guild, TextChannel } from "discord.js";
import { Formatter } from "formatters/Formatter";
import { MappedObject } from "utils/FilterUtil";
import Transporter from "transporters/Transporter";

interface DiscordRelativeTransporterOptions {
    module: Module;
    channelName: string;
    guildIdProperty: string;
    formatter: Formatter;
}

class DiscordRelativeTransporter extends Transporter {
    public readonly module: Module;
    public readonly channelName: string;
    public readonly guildIdProperty: string;

    constructor(options: DiscordRelativeTransporterOptions) {
        super({
            formatter: options.formatter
        });

        this.module = options.module;
        this.channelName = options.channelName;
        this.guildIdProperty = options.guildIdProperty;
    }

    public async transport(
        originalObject: MappedObject,
        refinedObject: MappedObject
    ): Promise<void> {
        const guildId: string | undefined =
            originalObject[this.guildIdProperty] ?? undefined;
        const guild: Guild | undefined = this.module.client.guilds.cache.find(
            (g) => g.id === guildId
        );

        if (guild == undefined) {
            return Promise.reject(`Guild with id '${guildId}' undefined.`);
        }

        const channel: Channel | undefined = guild.channels.cache.find(
            (c) => c.name === this.channelName
        );

        if (channel == undefined) {
            return Promise.reject(
                `Channel with name '${this.channelName}' undefined within guild ${guild.name}.`
            );
        } else if (!channel.isText()) {
            return Promise.reject(
                `Channel with name '${this.channelName}' not a valid text channel.`
            );
        }

        const textChannel: TextChannel = channel as TextChannel;

        textChannel.send(this.formatter(refinedObject)).catch(console.error);

        return Promise.resolve();
    }
}

export default DiscordRelativeTransporter;
