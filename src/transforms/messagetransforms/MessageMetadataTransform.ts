import {
    GuildMember,
    Message,
    NewsChannel,
    Role,
    TextChannel,
    ThreadChannel
} from "discord.js";
import { MappedObject } from "utils/FilterUtil";

import Transform from "transforms/Transform";

class MessageMetadataTransform extends Transform {
    public apply(object: MappedObject): MappedObject {
        const message: Message = object as Message;

        const guild = message.guild;
        const channel = message.channel as
            | TextChannel
            | NewsChannel
            | ThreadChannel;
        const author = message.author;
        const member = message.member;
        const mentions = message.mentions;

        const transformedProperties = {
            id: message.id,
            guild: {
                id: guild?.id ?? null,
                name: guild?.name ?? null
            },
            channel: {
                id: channel.id,
                name: channel.name ?? null,
                parent: channel.parent
                    ? {
                          id: channel.parent.id,
                          name: channel.parent.name
                      }
                    : null
            },
            author: {
                id: author.id,
                username: author.username,
                nickname: member?.nickname ?? null
            },
            createdAt: message.createdAt,
            type: message.type,
            mentions: {
                everyone: mentions.everyone,
                members:
                    mentions.members?.map((m: GuildMember) => {
                        return {
                            id: m.id,
                            username: m.user.username,
                            nickname: m.nickname ?? null
                        };
                    }) ?? [],
                roles:
                    mentions.roles?.map((r: Role) => {
                        return {
                            id: r.id,
                            name: r.name
                        };
                    }) ?? [],
                channels: mentions.channels?.map((c: MappedObject) => {
                    return {
                        id: c.id,
                        name: c.name ?? null,
                        parent: c?.parent
                            ? {
                                  id: c?.parent.id,
                                  name: c?.parent?.name ?? null
                              }
                            : null
                    };
                })
            },
            editedAt: object.editedAt //I don't understand what's happening well enough to know why this is necessary
        };

        return { ...object, ...transformedProperties };
    }
}

export default MessageMetadataTransform;
