import {
    Message
} from "discord.js";
import { MappedObject } from "utils/FilterUtil";

import Transform from "transforms/Transform";

class MessageDeleteTransform extends Transform {
    public apply(object: MappedObject): MappedObject {
        const message: Message = object as Message;

        //const guild = message.guild;

        const transformedProperties = {
            //id: message.id,
            //guild: {
            //    id: guild?.id ?? null,
            //    name: guild?.name ?? null
            //},
            deletedAt: new Date()
        };

        return { ...object, ...transformedProperties };
    }
}

export default MessageDeleteTransform;
