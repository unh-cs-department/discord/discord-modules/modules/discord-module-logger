import { MappedObject } from "utils/FilterUtil";

abstract class Transform {
    abstract apply(data: MappedObject): MappedObject;
}

export default Transform;
