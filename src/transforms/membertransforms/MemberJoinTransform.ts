import {
    GuildMember
} from "discord.js";
import { MappedObject } from "utils/FilterUtil";

import MemberTransform from "./MemberTransform";

class MemberJoinTransform extends MemberTransform {
    public override apply(object: MappedObject): MappedObject {
        const member = object as GuildMember;
        const obj = super.apply(object);

        return { ...obj, joinedAt: member.joinedAt };
    }
}

export default MemberJoinTransform
