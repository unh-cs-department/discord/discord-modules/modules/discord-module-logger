import {
    GuildMember
} from "discord.js";
import { MappedObject } from "utils/FilterUtil";

import Transform from "transforms/Transform";

abstract class MemberTransform extends Transform {
    public apply(object: MappedObject): MappedObject {
        const member = object as GuildMember;
        const guild = member.guild;

        const transformedProperties = {
            user: {
                id: member.id,
                username: member.user.username,
                nickname: member.displayName
            },
            guild: {
                id: guild?.id ?? null,
                name: guild?.name ?? null
            },
            username: member.user.username,
        };

        return { ...object, ...transformedProperties };
    }
}

export default MemberTransform
